import { useContext } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Banner ({data}) {
	// console.log(data)
	const { title, content, destination, label } = data;
	const { user } = useContext(UserContext);

	return (
		<Row>
			<Col className="p-5">
				<Card className="text-center">
		      		<Card.Header>"All things bright and beautiful, like cars and trucks are great and small."</Card.Header>
		      		<Card.Body className="banner">
		        		<Card.Title>{title}</Card.Title>
		        		<Card.Text>
		          			{content}
		        		</Card.Text>
		        		{
		        			(user.isAdmin) ?
		        				<Button variant="primary" as={Link} to="/products">Welcome Admin! Show car list here.</Button>
		          			:
		          				<Button variant="primary" as={Link} to={destination}>{label}</Button>
		         		}

		      		</Card.Body>
		    	</Card>
		    </Col>
		</Row>
	)
};