import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [fullName, setFullName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether the submit button is enabled or disabled
	// Conditional rendering
	const [isActive, setIsActive] = useState(false);

	function registerUser (e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Kindly provide another email to complete registration."
				})
			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						fullName: fullName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true) {

						// Clear input fields
						setFullName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Ready to get your product?"
						})

						navigate("/login");

					} else {

						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please, try again."
						})
					}

				})
			}
		})
	}


	useEffect(() => {
		if((fullName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [fullName, email, mobileNo, password1, password2])


	return(

		(user.id !== null) ?
			<Navigate to="/products"/>

			:

			<Form onSubmit={(e) => registerUser(e)} className="page-container" >

				  <h1 className="text-center" style={{ color: 'white' }}>Registration</h1>

				  <Form.Group className="mb-3" controlId="fullName">
				    <Form.Label>Full Name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={fullName}
				    	onChange={(e) => {setFullName(e.target.value)}}
				    	placeholder="Enter your Full Name" 
				    	required
				    	/>
				  </Form.Group>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			        <Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value={mobileNo}
			        	onChange={(e) => {setMobileNo(e.target.value)}}
			        	placeholder="0999999999" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password1}
			        	onChange={(e) => {setPassword1(e.target.value)}}
			        	placeholder="Enter Your Password" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password2}
			        	onChange={(e) => {setPassword2(e.target.value)}}
			        	placeholder="Verify Your Password" />
			      </Form.Group>
			      { isActive ?
			      			<Button variant="primary" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      			:
			      			<Button variant="primary" type="submit" id="submitBtn" disabled>
			      			  Submit
			      			</Button>
			      }			     
			    </Form>	
	)
}
