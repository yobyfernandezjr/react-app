import Banner from '../components/Banner';

export default function PageNotFound () {

	const data = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found!",
		destination: "/",
		label: "Back to home"
	}
	return (
		<Banner data={data} />
	)
};